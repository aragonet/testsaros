package testsaros;

import static org.junit.Assert.*;

import org.junit.Test;

public class Prueba {

	@Test
	public void test() {
		Sumador sumador = new Sumador();
		sumador.suma(2,4);
		assertEquals(6, sumador.getResult());
	}

	@Test
	public void test2() {
		Sumador sumador = new Sumador();
		sumador.factorial(5);
		assertEquals(120, sumador.getResult());
	}
	
	@Test
	public void test3() {
		Sumador sumador = new Sumador();
		sumador.factorial(0);
		assertEquals(1, sumador.getResult());
	}
}
